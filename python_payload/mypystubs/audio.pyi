def set_volume_dB(v: float) -> None:
    pass

def get_volume_dB() -> float:
    pass

def adjust_volume_dB(v: float) -> float:
    pass

def headphones_are_connected() -> bool:
    pass

def set_mute(v: bool) -> None:
    pass

def get_mute() -> bool:
    pass
