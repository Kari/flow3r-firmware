.. _Badge Link API:

``badge_link`` module
=====================

.. note::

   See also: :ref:`Badge link overview<Badge link>`

.. automodule:: badge_link
   :members:
   :undoc-members:
